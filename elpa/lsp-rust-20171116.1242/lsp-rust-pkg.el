;;; -*- no-byte-compile: t -*-
(define-package "lsp-rust" "20171116.1242" "Rust support for lsp-mode" '((lsp-mode "3.0") (rust-mode "0.3.0")) :commit "26a616bd7c69bb73d2ef2d033de53f613770a256" :url "https://github.com/emacs-lsp/lsp-rust" :keywords '("rust"))
